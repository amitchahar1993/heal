
import React from 'react';
import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from "react-navigation";
import { AppLoading } from 'expo'
import NavigationService from './app/Service/Navigation'
import SignUp from './app/Screen/Account/SignUp/signUp';
import EditAccount from './app/Screen/Account/EditAccount/EditAccount';
import Condition from './app/Screen/Account/Settings/Condition';
import Accounts from './app/Screen/Account/Account';
import Tolerence from './app/Screen/Account/Settings/Tolerance';
import Role from './app/Screen/Account/Settings/Roles';
import EditCondition from './app/Screen/Account/EditSettings/EditConditions'
import * as Font from 'expo-font';

let customFonts = {

  RobotoRegular: require('./assets/Fonts/Roboto-Regular.ttf'),
  RobotoBold: require('./assets/Fonts/Roboto-Bold.ttf'),
  RobotoSemiBold: require('./assets/Fonts/Roboto-Medium.ttf'),
  RobotoLight: require('./assets/Fonts/Roboto-Light.ttf'),
}
const AppNav = createStackNavigator({
  SignUp: {
    screen: SignUp
  },
  Accounts: {
    screen: Accounts
  },
  EditAccount: {
    screen: EditAccount
  },

  Condition: {
    screen: Condition
  },
  Tolerence: {
    screen: Tolerence
  },
  Role: {
    screen: Role
  },
  EditCondition: {
    screen: EditCondition
  },
},
  {
    headerMode: 'none',
    initialRouteName: 'SignUp',
  })
const AppContainer = createAppContainer(AppNav);

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      fontsLoaded: false,
    };


  }
  async _loadFontsAsync() {
    await Font.loadAsync(customFonts);
    this.setState({ fontsLoaded: true });
  }
  componentDidMount() {
    this._loadFontsAsync();
  }
  render() {
    if (this.state.fontsLoaded) {
      return (

        <AppContainer
          ref={(r) => {
            NavigationService.setTopLevelNavigator(r);
          }}

        />
      );
    }
    else {
      return <AppLoading />;
    }
  }

}




