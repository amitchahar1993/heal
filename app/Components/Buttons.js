import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, Dimensions } from 'react-native'
const { width, height } = Dimensions.get('window');
export default class App extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    const { text, onPress, colors, width } = this.props;
    return (

      <TouchableOpacity onPress={onPress}>
        <View style={[styles.button, { backgroundColor: colors, width: width }]}>

          <Text style={styles.text}>{text} </Text>
        </View>
      </TouchableOpacity>

    )
  }/*  */
}
const styles = StyleSheet.create({
  button: {

    borderRadius: 8,
    paddingVertical: 14,
    paddingHorizontal: 10,
    backgroundColor: '#f01d71'
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    fontSize: 16,
    textAlign: 'center'
  }
})