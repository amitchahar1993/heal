import React, { Component } from 'react'
import { Text, StyleSheet, View, StatusBar, Dimensions, SafeAreaView, TextInput } from 'react-native'
const { height, width } = Dimensions.get('window');
import { FontAwesome } from '@expo/vector-icons';
import { Content, Container } from 'native-base';
import { Formik } from 'formik';
import Buttons from '../../../Components/Buttons'
export default class signUp extends Component {
  render() {
    return (
      <Container>
        <StatusBar backgroundColor='#FFF' animated barStyle='dark-content' />
        <SafeAreaView>
          <View style={styles.container}>

            <FontAwesome name="user-circle-o" size={width * 0.25} color="grey" />

            <View>
              <Text style={styles.createText}>
                Create Account
            </Text>
            </View>
            <View>
              <Text styel={styles.fillText}>Fill out below fields</Text>
            </View>

            <Formik initialValues={{
              name: '', phone: '', dob: ''
            }}
              onSubmit={(values) => {

              }}>
              {(props) => (
                <View>
                  <TextInput
                    style={styles.textInput}
                    placeholder='Full Name'
                    onChangeText={props.handleChange('name')}
                    value={props.values.name} />
                  <TextInput
                    style={styles.textInput}
                    placeholder='Mobile Phone'
                    onChangeText={props.handleChange('phone')}
                    value={props.values.phone} />
                  <TextInput
                    style={styles.textInput}
                    placeholder='Date of Birth'
                    onChangeText={props.handleChange('dob')}
                    value={props.values.dob} />
                  <Buttons text="create" onPress={this.handle} width={width} colors='#0000ff' />
                </View>
              )}
            </Formik>

          </View>
        </SafeAreaView>

      </Container>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  icon: {
    paddingVertical: 15,
    paddingHorizontal: 10
  },
  createText: {

    fontFamily: 'RobotoBold',
    color: '#0000ff',
    fontSize: 30
  },
  fillText: {
    fontFamily: 'RobotoMedium',
    fontSize: 18,
    color: '#000'
  },
  textInput: {
    borderBottomColor: "#ddd",
    borderBottomWidth: 1.5,
    padding: 10,
    fontSize: 40,
    fontFamily: 'RobotoRegular'
  }

})
